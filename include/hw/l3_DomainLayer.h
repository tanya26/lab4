#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"
#include <sstream>

const int MAX_NAME = 100;
const int MIN_PRICE = 0;
const int MAX_PRICE = 200000;
const int MIN_CAL = 0;
const int MAX_CAL = 10000;
const int MIN_TIME = 0;
const int MAX_TIME = 120;
const int MIN_POPULAR= 0;
const int MAX_POPULAR= 5;

class Calories {
 public:
  Calories(int calories, std::string name);
  int getCalories() const;
  std::string getName() const;
 private:
  int _calories;
  std::string _name;
};

class FastFoodRestaurant : public ICollectable {
 protected:
  bool invariant() const;
 public:
  FastFoodRestaurant() = delete;
  FastFoodRestaurant(const FastFoodRestaurant & p) = delete;
  FastFoodRestaurant & operator = (const FastFoodRestaurant & p) = delete;
  FastFoodRestaurant(std::string name, int price, int time, int popular);
  FastFoodRestaurant(std::string name, int price, int time, int popular, std::vector<Calories> cal);
  const std::string& getName() const;
  int getPrice() const;
  int getCalories() const;
  int getTime() const;
  int getPopular() const;
  bool write(std::ostream& os) override;
  std::vector<Calories> FastFoodCalories() const;
  void addProductCalories(int calories, const std::string& name);
 private:
  std::string _name;
  int _price;
  int _time;
  int _popular;
  std::vector<Calories> _products;
};

class ItemCollector: public ACollector {
 public:
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
  void addProductCalories(int index, int calories, const std::string& name);
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
