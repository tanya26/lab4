#include "hw/l2_ApplicationLayer.h"

bool Application::performCommand(const std::vector<std::string> & args) {
  if (args.empty()) {
	return false;
  }

  if (args[0] == "l" || args[0] == "load") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!_col.loadCollection(filename)) {
	  _out.Output("Ошибка при загрузке файла '" + filename + "'");
	  return false;
	}
	return true;
  }

  if (args[0] == "s" || args[0] == "save") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!_col.saveCollection(filename)) {
	  _out.Output("Ошибка при сохранении файла '" + filename + "'");
	  return false;
	}
	return true;
  }

  if (args[0] == "c" || args[0] == "clean") {
	if (args.size() != 1) {
	  _out.Output("Некорректное количество аргументов команды clean");
	  return false;
	}
	_col.clean();
	return true;
  }

  if (args[0] == "a" || args[0] == "add") {
	if (args.size() != 5) {
	  _out.Output("Некорректное количество аргументов команды add");
	  return false;
	}

	_col.addItem(std::make_shared<FastFoodRestaurant>(args[1], std::stoi(args[2]), std::stoi(args[3]), std::stoi(args[4])));
	return true;
  }

  if (args[0] == "ap" || args[0] == "add_product") {
	if (args.size() != 4) {
	  _out.Output("Некорректное количество аргументов команды add_product");
	  return false;
	}

	_col.addProductCalories(std::stoi(args[1]), std::stoi(args[2]), args[3]);
	return true;
  }

  if (args[0] == "r" || args[0] == "remove") {
	if (args.size() != 2) {
	  _out.Output("Некорректное количество аргументов команды remove");
	  return false;
	}
	_col.removeItem(stoul(args[1]));
	return true;
  }

  if (args[0] == "u" || args[0] == "update") {
	if (args.size() != 6) {
	  _out.Output("Некорректное количество аргументов команды update");
	  return false;
	}
	_col.updateItem(stoul(args[1]), std::make_shared<FastFoodRestaurant>(args[2], std::stoi(args[3]), std::stoi(args[4]), std::stoi(args[5])));
	return true;
  }

  if (args[0] == "v" || args[0] == "view") {
	if (args.size() != 1) {
	  _out.Output("Некорректное количество аргументов команды view");
	  return false;
	}

	size_t count = 0;
	for(size_t i = 0; i < _col.getSize(); ++i) {
	  const FastFoodRestaurant & item = dynamic_cast<FastFoodRestaurant &>(*_col.getItem(i));
	  if (!_col.isRemoved(i)) {
		_out.Output("[" + std::to_string(i) + "] "
		+ "Наименование блюда: " + item.getName()
		+ " Стоимость: " + std::to_string(item.getPrice())
		+ " Количество калорий: " + std::to_string(item.getCalories())
		+ " Время приготовлени(мин.): " + std::to_string(item.getTime())
		+ " Популярность: " + std::to_string(item.getPopular()));
		count ++;
	  }
	}

	_out.Output("Количество элементов в коллекции: " + std::to_string(count));
	return true;
  }
  if (args[0] == "vp" || args[0] == "view_product") {
	if (args.size() != 2) {
	  _out.Output("Некорректное количество аргументов команды view_product");
	  return false;
	}

	const FastFoodRestaurant & item = dynamic_cast<FastFoodRestaurant &>(*_col.getItem(std::stoi(args[1])));
	if (!_col.isRemoved(std::stoi(args[1]))) {
	  _out.Output("Название: " + item.getName());
	  auto products = item.FastFoodCalories();
	  for (const auto& item_p: products) {
		_out.Output("Название ингредиента: " + item_p.getName());
		_out.Output("Калорийность: " + std::to_string(item_p.getCalories()));
	  }
	  _out.Output("Общая Калорийность: " + std::to_string(item.getCalories()));
	}

	return true;
  }

  _out.Output("Недопустимая команда '" + args[0] + "'");
  return false;
}
