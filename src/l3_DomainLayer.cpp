#include "hw/l3_DomainLayer.h"

#include <utility>

bool FastFoodRestaurant::invariant() const {
  return _name.size() <= MAX_NAME && _price >= MIN_PRICE && _price <= MAX_PRICE && getCalories() >= MIN_CAL && getCalories() <= MAX_CAL
	  && _time >= MIN_TIME && _time <= MAX_TIME && _popular >= MIN_POPULAR && _popular <= MAX_POPULAR;
}

FastFoodRestaurant::FastFoodRestaurant(std::string name, int price, int time, int popular): _name(std::move(name)), _price(price), _time(time), _popular(popular)  {
  assert(invariant());
}

const std::string& FastFoodRestaurant::getName() const {
  return _name;
}

int FastFoodRestaurant::getPrice() const {
  return _price;
}

int FastFoodRestaurant::getCalories() const {
  int calories = 0;
  for (const auto& item: _products) {
	calories += item.getCalories();
  }
  return calories;
}

int FastFoodRestaurant::getTime() const {
  return _time;
}

int FastFoodRestaurant::getPopular() const {
  return _popular;
}

bool FastFoodRestaurant::write(std::ostream& os) {
  writeString(os, _name);
  writeNumber(os, _price);
  writeNumber(os, _time);
  writeNumber(os, _popular);
  writeNumber(os, _products.size());
  for (auto& item: _products) {
	writeString(os, item.getName());
	writeNumber(os, item.getCalories());
  }
  return os.good();
}

void FastFoodRestaurant::addProductCalories(int calories, const std::string& name) {
  _products.emplace_back(calories, name);
}

std::vector<Calories> FastFoodRestaurant::FastFoodCalories() const {
  return _products;
}
FastFoodRestaurant::FastFoodRestaurant(std::string name, int price, int time, int popular, std::vector<Calories> cal): _time(time), _price(price), _popular(popular), _name(std::move(name)), _products(std::move(cal)) {
  assert(invariant());
}

std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  std::string name = readString(is, MAX_NAME);
  int price = readNumber<int>(is);
  int time = readNumber<int>(is);
  int popular = readNumber<int>(is);
  auto count =  readNumber<size_t>(is);
  std::vector<Calories> calories;
  for (int kI = 0; kI < count; ++kI) {
	std::string name_p = readString(is, MAX_NAME);
	int cal = readNumber<int>(is);
	calories.emplace_back(cal, name_p);
  }
  return std::make_shared<FastFoodRestaurant>(name, price, time, popular, calories);
}

void ItemCollector::addProductCalories(int index, int calories, const std::string& name) {
  FastFoodRestaurant & item = dynamic_cast<FastFoodRestaurant &>(*getItem(index));
  item.addProductCalories(calories, name);
}

int Calories::getCalories() const {
  return _calories;
}

Calories::Calories(int calories, std::string name): _calories(calories), _name(std::move(name)) {}

std::string Calories::getName() const {
  return _name;
}
